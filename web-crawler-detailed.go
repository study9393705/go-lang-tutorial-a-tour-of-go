// Package Declaration:
package main
// This line declares that this Go file is part of the main package.


// Import Statements:
import (
   "fmt"
   "sync"
)
// Imports the necessary packages. fmt is for formatted I/O, and sync is for synchronization,
// including sync.Mutex and sync.WaitGroup.


// Fetcher Interface:
type Fetcher interface {
   Fetch(url string) (body string, urls []string, err error)
}
// Declares an interface Fetcher that defines a Fetch method.
// Any type implementing this method can be used as a fetcher in the Crawl function.


// SafeCache Struct:
type SafeCache struct {
   mu    sync.Mutex
   cache map[string]bool
}
// Defines a struct SafeCache with a mutex (mu) and a map (cache).
// The mutex is used to make the map safe for concurrent access.


// NewSafeCache Function:
func NewSafeCache() *SafeCache {
   return &SafeCache{cache: make(map[string]bool)}
}
// Creates and returns a new instance of SafeCache.


// Crawl Function:
func Crawl(url string, depth int, fetcher Fetcher, cache *SafeCache, wg *sync.WaitGroup) {
The Crawl function is the main recursive function for crawling URLs. It takes a URL, depth, fetcher, cache, and a wait group as parameters.


// Defer Statement:
defer wg.Done()
// The defer statement ensures that wg.Done() is called when the function exits, which helps in signaling that the goroutine is done.

// Locking and Checking Cache:
cache.mu.Lock()
if cache.cache[url] {
   cache.mu.Unlock()
   return
}
// Locks the mutex to access the cache safely. Checks if the URL has already been fetched. If yes, unlocks the mutex and returns.


// Marking URL as Fetched:
cache.cache[url] = true
cache.mu.Unlock()
// Marks the URL as fetched in the cache and unlocks the mutex.

// Fetching the URL:
body, urls, err := fetcher.Fetch(url)
// Fetches the URL using the Fetch method of the provided fetcher.

// Printing Found URL:
fmt.Printf("found: %s %q\n", url, body)
// Prints the found URL and its body.

// Parallel Fetching of URLs:
var childWg sync.WaitGroup
for _, u := range urls {
   childWg.Add(1)
   go Crawl(u, depth-1, fetcher, cache, &childWg)
}
childWg.Wait()
// Creates a wait group for the child goroutines, iterates through the URLs found on the current page,
// and fetches them in parallel by creating new goroutines.

// Main Function:
func main() {
   cache := NewSafeCache()
   var wg sync.WaitGroup
   wg.Add(1)
   Crawl("https://golang.org/", 4, fetcher, cache, &wg)
   wg.Wait()
}
// Creates a new cache, a wait group, and initiates the crawling process with the main URL.

// FakeFetcher and fetcher:
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
   body string
   urls []string
}
// Defines a fake fetcher and fake result types for testing purposes.

// fetcher Initialization:
var fetcher = fakeFetcher{
   // ... fake results for different URLs
}
// Initializes a fake fetcher with predefined results for specific URLs.

// This code implements a concurrent web crawler using goroutines to fetch URLs in parallel
// and a cache to avoid fetching the same URL multiple times. The sync.Mutex is used to ensure
// safe access to the cache, and sync.WaitGroup is used to wait for all the goroutines to finish
// before returning from the Crawl function.