package main

import (
	"fmt"
	"sync"
	"time"
)
// Here, you're defining a Go program. You import necessary packages: fmt for formatted I/O,
// sync for synchronization, and time for dealing with time-related operations.

// SafeCounter is safe to use concurrently.
type SafeCounter struct {
	mu sync.Mutex
	v  map[string]int
}
// You define a type named SafeCounter with two fields: mu, a sync.Mutex for managing mutual
// exclusion to ensure safe concurrent access, and v, a map that associates strings with integers.

// Inc increments the counter for the given key.
func (c *SafeCounter) Inc(key string) {
	c.mu.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	c.v[key]++
	c.mu.Unlock()
}
// You define a method Inc for SafeCounter, which increments the counter associated with a given key.
// It uses the mutex mu to lock access to the map v while updating the counter.

// Value returns the current value of the counter for the given key.
func (c *SafeCounter) Value(key string) int {
	c.mu.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	defer c.mu.Unlock()
	return c.v[key]
}
// You define another method Value for SafeCounter, which returns the current value of the counter
// for a given key. Similar to Inc, it locks and unlocks the mutex mu to ensure safe access to the map v.

func main() {
	c := SafeCounter{v: make(map[string]int)}
// In the main function, you create an instance of SafeCounter named c with an initialized map.

	for i := 0; i < 1000; i++ {
		go c.Inc("somekey")
	}
// In this loop, you launch 1000 goroutines concurrently, and each goroutine calls the Inc method
// on the SafeCounter instance, incrementing the counter associated with the key "somekey".

	time.Sleep(time.Second)
// The main function sleeps for one second to allow all the goroutines to finish their execution.
// This is done to ensure that the counter increments are completed before printing the final result.

	fmt.Println(c.Value("somekey"))
}
// Finally, the main function prints the current value of the counter associated with the key "somekey"
// using the Value method of the SafeCounter instance.

// In summary, this program demonstrates safe concurrent access to a map by using a mutex to synchronize
// access to the map's data structure. The SafeCounter type ensures that the Inc and Value methods
// can be safely called from multiple goroutines without causing data races.