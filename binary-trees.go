package main

import (
	"fmt"
	"golang.org/x/tour/tree"
)

// 1. Walk Function:
func Walk(t *tree.Tree, ch chan int) {
    if t == nil {
        return
    }

    // Walk left subtree
    Walk(t.Left, ch)

    // Send value to channel
    ch <- t.Value

    // Walk right subtree
    Walk(t.Right, ch)
}
// This function is designed to perform an in-order traversal of a binary tree (t) and send its values to a channel (ch).
// It uses recursion to traverse the left subtree, send the current node's value to the channel, and then traverse the right subtree.
// The base case checks if the current node is nil (representing an empty subtree), in which case it returns.

// 2. Same Function:
func Same(t1, t2 *tree.Tree) bool {
    ch1, ch2 := make(chan int), make(chan int)

    go Walk(t1, ch1)
    go Walk(t2, ch2)

    for i := 0; i < 10; i++ {
        val1, val2 := <-ch1, <-ch2
        if val1 != val2 {
            return false
        }
    }

    return true
}
// The Same function determines whether two trees (t1 and t2) store the same sequence of values.
// It creates two channels (ch1 and ch2) for the two trees.
// It launches two goroutines to concurrently walk each tree using the Walk function.
// It then reads values from both channels and compares them. If any pair of values is different,
// it returns false. If all values match, it returns true.

// 3. main Function:
func main() {
    // Test the Walk function
    ch := make(chan int)
    go Walk(tree.New(1), ch)

    for i := 0; i < 10; i++ {
        fmt.Println(<-ch)
    }

    // Test the Same function
    fmt.Println(Same(tree.New(1), tree.New(1))) // Should return true
    fmt.Println(Same(tree.New(1), tree.New(2))) // Should return false
}
// The main function tests the Walk and Same functions.
// It creates a channel (ch) and starts a goroutine to walk a tree (tree.New(1)) using the Walk function.
// It prints the first 10 values received from the channel, effectively testing the Walk function.
// It then tests the Same function with two trees (tree.New(1) and tree.New(2)) and prints the results.
// This code demonstrates the use of Go's concurrency and channels to traverse binary trees and compare
// their sequences of values. The Walk function generates the values of a tree, and the Same function checks
// whether two trees have the same sequence of values.