package main

// List represents a singly-linked list that holds
// values of any type.
type List[T any] struct {
	next *List[T]
	val  T
}

func main() {
}


// Insert at the Beginning:
// Create a method to insert a new element at the beginning of the list.
func (l *List[T]) insertAtBeginning(value T) *List[T] {
	newNode := &List[T]{val: value, next: l}
	return newNode
}

// Insert at the End:
// Create a method to insert a new element at the end of the list.
func (l *List[T]) insertAtEnd(value T) *List[T] {
	newNode := &List[T]{val: value}
	if l == nil {
		return newNode
	}

	current := l
	for current.next != nil {
		current = current.next
	}
	current.next = newNode
	return l
}

// Delete a Node:
// Create a method to delete a node with a specific value from the list.
func (l *List[T]) deleteNode(value T) *List[T] {
	if l == nil {
		return nil
	}

	if l.val == value {
		return l.next
	}

	current := l
	for current.next != nil && current.next.val != value {
		current = current.next
	}

	if current.next != nil {
		current.next = current.next.next
	}

	return l
}

// Search for a Value:
// Create a method to search for a specific value in the list.
func (l *List[T]) search(value T) bool {
	current := l
	for current != nil {
		if current.val == value {
			return true
		}
		current = current.next
	}
	return false
}