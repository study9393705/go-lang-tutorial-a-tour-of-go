// Let's go through the code step by step, explaining each line:

package main

import (
	"fmt"
)
// package main: Declares that this Go file belongs to the main package, which is necessary for creating an executable program.
// import "fmt": Imports the "fmt" package, which provides functions for formatting and printing to the console.


// List represents a singly-linked list that holds
// values of any type.
type List[T any] struct {
	next *List[T]
	val  T
}
// type List[T any] struct: Defines a generic type List representing a singly-linked list. It has two fields:
// next: A pointer to the next node in the list.
// val: The value of the current node.


// Insert at the Beginning
func (l *List[T]) insertAtBeginning(value T) *List[T] {
	newNode := &List[T]{val: value, next: l}
	return newNode
}
// func (l *List[T]) insertAtBeginning(value T) *List[T]: Defines a method on the List type for inserting a new node at the beginning of the list.
// newNode := &List[T]{val: value, next: l}: Creates a new node with the given value and sets its next pointer to the current head of the list.
// return newNode: Returns the new node, effectively making it the new head of the list.


// Insert at the End
func (l *List[T]) insertAtEnd(value T) *List[T] {
	newNode := &List[T]{val: value}
	if l == nil {
		return newNode
	}

	current := l
	for current.next != nil {
		current = current.next
	}
	current.next = newNode
	return l
}
// func (l *List[T]) insertAtEnd(value T) *List[T]: Defines a method for inserting a new node at the end of the list.
// newNode := &List[T]{val: value}: Creates a new node with the given value.
// if l == nil { return newNode }: Checks if the list is empty. If it is, the new node becomes the entire list.
// The loop for current.next != nil { current = current.next } iterates through the list to find the last node.
// current.next = newNode: Appends the new node to the end of the list.
// return l: Returns the original head of the list.


// Delete a Node
func (l *List[T]) deleteNode(value T) *List[T] {
	if l == nil {
		return nil
	}

	if l.val == value {
		return l.next
	}

	current := l
	for current.next != nil && current.next.val != value {
		current = current.next
	}

	if current.next != nil {
		current.next = current.next.next
	}

	return l
}
// func (l *List[T]) deleteNode(value T) *List[T]: Defines a method for deleting a node with a specific value from the list.
// if l == nil { return nil }: Checks if the list is empty. If it is, there's nothing to delete.
// if l.val == value { return l.next }: Checks if the head of the list contains the value to be deleted. If true, returns the rest of the list (skipping the head).
// The loop for current.next != nil && current.next.val != value { current = current.next } iterates through the list to find the node to be deleted.
// current.next = current.next.next: Removes the node with the specified value by adjusting the next pointer.
// return l: Returns the original head of the list.


// Search for a Value
func (l *List[T]) search(value T) bool {
	current := l
	for current != nil {
		if current.val == value {
			return true
		}
		current = current.next
	}
	return false
}
// func (l *List[T]) search(value T) bool: Defines a method for searching a value in the list.
// The loop for current != nil { if current.val == value { return true } current = current.next } iterates through the list, checking if each node contains the specified value.
// return false: Returns false if the value is not found in the list.


func main() {
	// Creating an empty list
	var myList *List[int]

	// Inserting values
	myList = myList.insertAtBeginning(10)
	myList = myList.insertAtEnd(20)
	myList = myList.insertAtEnd(30)

	// Displaying the list
	displayList(myList)

	// Searching for a value
	found := myList.search(20)
	fmt.Printf("Value 20 found: %t\n", found)

	// Deleting a node
	myList = myList.deleteNode(20)

	// Displaying the modified list
	displayList(myList)
}
// var myList *List[int]: Declares an empty list.
// myList = myList.insertAtBeginning(10): Inserts the value 10 at the beginning of the list.
// myList = myList.insertAtEnd(20): Inserts the value 20 at the end of the list.
// myList = myList.insertAtEnd(30): Inserts the value 30 at the end of the list.
// displayList(myList): Displays the current state of the list.
// found := myList.search(20): Searches for the value 20 in the list.
// fmt.Printf("Value 20 found: %t\n", found): Prints whether the value 20 is found in the list.
// myList = myList.deleteNode(20): Deletes the node containing the value 20.
// displayList(myList): Displays the modified list after deletion.


// displayList is a helper function to display the elements of the list.
func displayList[T any](l *List[T]) {
	fmt.Print("List: ")
	for l != nil {
		fmt.Printf("%v -> ", l.val)
		l = l.next
	}
	fmt.Println("nil")
}
// func displayList[T any](l *List[T]): Defines a helper function for displaying the elements of the list.
// The loop for l != nil { fmt.Printf("%v -> ", l.val) l = l.next } iterates through the list, printing each node's value.
// fmt.Println("nil"): Prints "nil" to signify the end of the list.
// This code demonstrates the creation, modification, and display of a singly-linked list using the provided functionalities. The main function acts as a test case for the list operations.