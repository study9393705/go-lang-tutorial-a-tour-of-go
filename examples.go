Insert at the Beginning:

This method adds a new element to the beginning of the list. It creates a new node with the given value and sets its next pointer to the current head of the list.

go
Copy code
func (l *List[T]) insertAtBeginning(value T) *List[T] {
	newNode := &List[T]{val: value, next: l}
	return newNode
}
Example:

go
Copy code
// Creating an empty list
var myList *List[int]

// Inserting 10 at the beginning
myList = myList.insertAtBeginning(10)
Insert at the End:

This method adds a new element to the end of the list. It iterates through the list to find the last node and appends the new node to it.

go
Copy code
func (l *List[T]) insertAtEnd(value T) *List[T] {
	newNode := &List[T]{val: value}
	if l == nil {
		return newNode
	}

	current := l
	for current.next != nil {
		current = current.next
	}
	current.next = newNode
	return l
}
Example:

go
Copy code
// Creating an empty list
var myList *List[int]

// Inserting 20 at the end
myList = myList.insertAtEnd(20)
Delete a Node:

This method deletes a node with a specific value from the list. It traverses the list to find the node with the given value and removes it.

go
Copy code
func (l *List[T]) deleteNode(value T) *List[T] {
	if l == nil {
		return nil
	}

	if l.val == value {
		return l.next
	}

	current := l
	for current.next != nil && current.next.val != value {
		current = current.next
	}

	if current.next != nil {
		current.next = current.next.next
	}

	return l
}
Example:

go
Copy code
// Creating a list with values 5, 10, 15
myList := &List[int]{val: 5, next: &List[int]{val: 10, next: &List[int]{val: 15}}}

// Deleting the node with value 10
myList = myList.deleteNode(10)
Search for a Value:

This method searches for a specific value in the list and returns true if found, false otherwise.

go
Copy code
func (l *List[T]) search(value T) bool {
	current := l
	for current != nil {
		if current.val == value {
			return true
		}
		current = current.next
	}
	return false
}
Example:

go
Copy code
// Creating a list with values 5, 10, 15
myList := &List[int]{val: 5, next: &List[int]{val: 10, next: &List[int]{val: 15}}}

// Searching for the value 10
found := myList.search(10) // Returns true