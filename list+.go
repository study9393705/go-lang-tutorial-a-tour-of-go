package main

import (
	"fmt"
)

// List represents a singly-linked list that holds
// values of any type.
type List[T any] struct {
	next *List[T]
	val  T
}

// Insert at the Beginning
func (l *List[T]) insertAtBeginning(value T) *List[T] {
	newNode := &List[T]{val: value, next: l}
	return newNode
}

// Insert at the End
func (l *List[T]) insertAtEnd(value T) *List[T] {
	newNode := &List[T]{val: value}
	if l == nil {
		return newNode
	}

	current := l
	for current.next != nil {
		current = current.next
	}
	current.next = newNode
	return l
}

// Delete a Node
func (l *List[T]) deleteNode(value T) *List[T] {
	if l == nil {
		return nil
	}

	if l.val == value {
		return l.next
	}

	current := l
	for current.next != nil && current.next.val != value {
		current = current.next
	}

	if current.next != nil {
		current.next = current.next.next
	}

	return l
}

// Search for a Value
func (l *List[T]) search(value T) bool {
	current := l
	for current != nil {
		if current.val == value {
			return true
		}
		current = current.next
	}
	return false
}

func main() {
	// Creating an empty list
	var myList *List[int]

	// Inserting values
	myList = myList.insertAtBeginning(10)
	myList = myList.insertAtEnd(20)
	myList = myList.insertAtEnd(30)

	// Displaying the list
	displayList(myList)

	// Searching for a value
	found := myList.search(20)
	fmt.Printf("Value 20 found: %t\n", found)

	// Deleting a node
	myList = myList.deleteNode(20)

	// Displaying the modified list
	displayList(myList)
}

// displayList is a helper function to display the elements of the list.
func displayList[T any](l *List[T]) {
	fmt.Print("List: ")
	for l != nil {
		fmt.Printf("%v -> ", l.val)
		l = l.next
	}
	fmt.Println("nil")
}